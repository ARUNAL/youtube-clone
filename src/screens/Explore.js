import React from "react";
import { StyleSheet, Text, FlatList, ScrollView, View } from "react-native";
import Constant from "expo-constants";
import Header from "../components/Header";
import Card from "../components/card";

import { useSelector } from "react-redux";
const LittleCard = ({ name }) => {
  return (
    <View
      style={{
        backgroundColor: "red",
        width: 120,
        width: 180,
        borderRadius: 4,
        marginTop: 10,
      }}
    >
      <Text
        style={{
          textAlign: "center",
          color: "white",
          fontSize: 22,
          marginTop: 5,
        }}
      >
        {name}
      </Text>
    </View>
  );
};

export default function Explore() {
  const cardData = useSelector((state) => {
    return state;
  });
  return (
    <View style={{ flex: 1, marginTop: Constant.statusBarHeight }}>
      <Header />
      <ScrollView>
        <View
          style={{
            flexDirection: "row",
            flexWrap: "wrap",
            justifyContent: "space-around",
          }}
        >
          <LittleCard name="Gameing" />
          <LittleCard name="music" />
          <LittleCard name="cinema" />
          <LittleCard name="tutorial" />
          <LittleCard name="fasion" />
          <LittleCard name="movie" />
        </View>
        <Text style={{ margin: 8, fontSize: 22, borderBottomWidth: 1 }}>
          Trending videos
        </Text>
        <FlatList
          data={cardData}
          renderItem={({ item }) => {
            return (
              <Card
                videoId={item.id.videoId}
                title={item.snippet.title}
                channel={item.snippet.channelTitle}
              />
            );
          }}
          keyExtractor={(item) => item.id.videoId}
        />
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({});
