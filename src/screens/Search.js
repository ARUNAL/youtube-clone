import React, { useState } from "react";
import {
  StyleSheet,
  TextInput,
  ScrollView,
  View,
  FlatList,
  ActivityIndicator,
} from "react-native";
import { Ionicons } from "@expo/vector-icons";
import MiniCard from "../components/MiniCard";
import Constant from "expo-constants";
import { useSelector, useDispatch } from "react-redux";

export default function Search({ navigation }) {
  const [value, setValue] = useState("");
  // const [MiniCardData, setMiniCardData] = useState([]);
  const dispatch = useDispatch();
  const MiniCardData = useSelector((state) => {
    return state;
  });

  const [loading, setLoading] = useState(false);
  const fetchData = () => {
    setLoading(true);
    fetch(
      `https://youtube.googleapis.com/youtube/v3/search?part=snippet&maxResults=10&q=${value}&type=video&key=AIzaSyDxLhdNCAAuzMDdlDPWpeK50xkUDQkYiUY`
    )
      .then((res) => res.json())
      .then((data) => {
        setLoading(false);
        dispatch({ type: "ADD", payload: data.items });
        // setMiniCardData(data.items);
      });
  };

  return (
    <View style={{ flex: 1, marginTop: Constant.statusBarHeight }}>
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-around",
          elevation: 5,
          backgroundColor: "white",
        }}
      >
        <Ionicons
          name="md-arrow-back"
          size={32}
          onPress={() => navigation.goBack()}
        />
        <TextInput
          style={{
            flex: 0.8,
            width: "70%",
            height: 40,
            backgroundColor: "#e6e6e6",
          }}
          value={value}
          onChangeText={(text) => setValue(text)}
        />
        <Ionicons name="md-send" size={32} onPress={() => fetchData()} />
      </View>
      {loading ? <ActivityIndicator size="large" color="red" /> : null}
      <FlatList
        data={MiniCardData}
        renderItem={({ item }) => {
          return (
            <MiniCard
              videoId={item.id.videoId}
              title={item.snippet.title}
              channel={item.snippet.channelTitle}
            />
          );
        }}
        keyExtractor={(item) => item.id.videoId}
      />
    </View>
  );
}

const styles = StyleSheet.create({});
