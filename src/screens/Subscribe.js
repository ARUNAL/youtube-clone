import React from "react";
import { StyleSheet, Text, View } from "react-native";
import Constant from "expo-constants";
import Header from "../components/Header";

const Subscribe = () => {
  return (
    <View style={{ flex: 1, marginTop: Constant.statusBarHeight }}>
      <Header />
      <Text>Subscribe video</Text>
    </View>
  );
};

export default Subscribe;

const styles = StyleSheet.create({});
