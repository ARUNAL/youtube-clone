import React from "react";
import { StyleSheet, FlatList,Animated, Text, View, ScrollView } from "react-native";
import Header from "../components/Header";
import Card from "../components/card";
import Constant from "expo-constants";
import { useSelector } from "react-redux";

export default function Home() {
const scrollY = new Animated.Value(0)
const diffClamp = Animated.diffClamp(scrollY,0,80)
const translateY = diffClamp.interpolate({
  inputRange:[0,45],
  outputRange:[0,-45]
})
  const cardData = useSelector((state) => {
    return state;
  });
  return (
    <View style={{ flex: 1, marginTop: Constant.statusBarHeight }}>
    <Animated.View style={{
      transform:[
      {translateY:translateY}
      ],
      elevation:2,}}>
    <Header />
    </Animated.View>
  
      <FlatList
        data={cardData}
        renderItem={({ item }) => {
          return (
            <Card
              videoId={item.id.videoId}
              title={item.snippet.title}
              channel={item.snippet.channelTitle}
            />
          );
        }}
        keyExtractor={(item) => item.id.videoId}
        onScroll={(e)=>{
          scrollY.setValue(e.nativeEvent.contentOffset.y)
        }}
      />
    </View>
  );
}

const styles = StyleSheet.create({});
