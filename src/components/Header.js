import React from "react";
import { StyleSheet, Text, View } from "react-native";
import Constant from "expo-constants";
import { AntDesign, Ionicons, MaterialIcons } from "@expo/vector-icons";
import { useNavigation } from "@react-navigation/native";
export default function Header() {
  const Navigation = useNavigation();
  const myColor = "#212121";

  return (
    <View
      style={{
        // marginTop: Constant.statusBarHeight,
        height: 40,
        Top:10,
        left:0,
        right:0,
        height:35,
        
        position:'absolute',
        backgroundColor: "white",
        flexDirection: "row",
        justifyContent: "space-between",
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
      }}
    >
      <View
        style={{
          flexDirection: "row",
          margin: 5,
        }}
      >
        <AntDesign
          style={{ marginLeft: 20 }}
          name="youtube"
          size={28}
          color="red"
        />
        <Text
          style={{
            fontSize: 22,
            color: myColor,
            marginLeft: 5,
            fontWeight: "bold",
          }}
        >
          YouTube
        </Text>
      </View>
      <View
        style={{
          margin: 5,
          flexDirection: "row",
          width: 150,
          justifyContent: "space-around",
        }}
      >
        <Ionicons name="md-videocam" color={myColor} size={28} />
        <Ionicons
          name="md-search"
          color={myColor}
          size={28}
          onPress={() => Navigation.navigate("Search")}
        />
        <MaterialIcons name="account-circle" color={myColor} size={28} />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({});
