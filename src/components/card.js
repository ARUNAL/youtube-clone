import React from "react";
import {
  StyleSheet,
  Image,
  Text,
  View,
  Dimensions,
  TouchableOpacity,
} from "react-native";
import { MaterialIcons } from "@expo/vector-icons";
import Constant from "expo-constants";
import { useNavigation } from "@react-navigation/native";
export default function Card(props) {
  const Navigation = useNavigation();
  return (
    <TouchableOpacity
      onPress={() =>
        Navigation.navigate("VideoPlayer", {
          videoId: props.VideoId,
          title: props.title,
        })
      }
    >
      <View style={{ marginBottom: 10, marginTop: Constant.statusBarHeight }}>
        <Image
          source={{
            uri: `https://i.ytimg.com/vi/${props.videoId}/hqdefault.jpg`,
          }}
          style={{
            width: "100%",
            height: 200,
          }}
        />
        <View
          style={{
            flexDirection: "row",
            margin: 5,
          }}
        >
          <MaterialIcons name="account-circle" size={32} color="#212121" />
          <View
            style={{
              marginLeft: 10,
            }}
          >
            <Text
              style={{
                fontSize: 20,

                width: Dimensions.get("screen").width - 50,
              }}
              ellipsizeMode="tail"
              numberOfLines={2}
            >
              {props.title}
            </Text>
            <Text>{props.channel}</Text>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({});
