import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
} from "react-native";
import Constant from "expo-constants";
import { useNavigation } from "@react-navigation/native";
export default function MiniCard(props) {
  const Navigation = useNavigation();
  return (
    <TouchableOpacity
      onPress={() =>
        Navigation.navigate("VideoPlayer", {
          videoId: props.VideoId,
          title: props.title,
        })
      }
    >
      <View
        style={{
          flexDirection: "row",
          margin: 10,
          marginBottom: 0,
          marginTop: Constant.statusBarHeight,
        }}
      >
        <Image
          source={{
            uri: `https://i.ytimg.com/vi/${props.videoId}/hqdefault.jpg`,
          }}
          style={{
            width: "45%",
            height: 100,
          }}
        />
        <View
          style={{
            paddingLeft: 7,
          }}
        >
          <Text
            style={{ fontSize: 15, width: Dimensions.get("screen").width / 2 }}
            ellipsizeMode="tail"
            numberOfLines={3}
          >
            {props.title}
          </Text>
          <Text style={{ fontSize: 12 }}>{props.channel}</Text>
        </View>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({});
